var express = require('express');
var bodyParser = require('body-parser');


let mongodb = require("mongodb");
let talentsprint = mongodb.MongoClient;


let fetch = express.Router().get("/", (req, res) => {
    talentsprint.connect("mongodb://127.0.0.1:27017/Eventplanner", (err, db) => {
        if (err) {
            throw err;
        }
        else {
            //console.log('fetch called...');
            db.collection("EventPlanner").find({}).toArray((err, array) => {
                if (err) {
                    throw err;
                }
                else {
                    if (array.length > 0) {
                        res.send(array);
                        console.log(array);
                        //data will be sent to client (browser or Angular)
                    } else {
                        res.send({ message: "Record Not Found..." });
                    }
                }
            });
        }
    });
});
module.exports = fetch;