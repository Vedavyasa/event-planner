import React from 'react'
import { Link } from 'react-router-dom/cjs/react-router-dom.min'

function weddingstages() {
  return (
    <div>
      <section id="stages">
        <h1><u><span>Models</span> of Wedding <span>Stages</span></u></h1>
        <div class="container">
          <div class="row">
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg1.jpeg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="wds.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="wedding.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stage.jpeg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="beachwedding.jpeg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg3.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg4.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <a href='../Booking' />
                <img src="stg2.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg5.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg6.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg7.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="stg8.jpg" class="card-img-top" alt="..." />
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3>Contact Us</h3>
              <p>123 Main Street, Karimnagar</p>
              <p>
                <i class="fa-solid fa-envelope"></i>Email: vedaeventplanner@gmail.com</p>
              <p>
                <i class="fa-solid fa-phone"></i>Phone: (+91) 99124**-***</p>
            </div>
            <div class="col-md-6">
              <h3>Follow Us</h3>
              <a href="#" class="social-icon">
                <i class="fa-brands fa-facebook"></i> Facebook</a>
              <a href="#" class="social-icon">
                <i class="fa-brands fa-twitter"></i>Twitter</a>
              <a href="#" class="social-icon">
                <i class="fa-brands fa-instagram"></i>Instagram</a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  )
}

export default weddingstages