import React, {useState}from 'react';
import { Link } from 'react-router-dom';
import { FaUserAlt } from 'react-icons/fa';



function Navbar() {
    const [showMenu, setshowMenu] = useState(false)
    const handleshowMenu = () => {
        setshowMenu(preve => !preve)
    }
    return (
        <div>
            <header className='header'>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <img className='logo' src='veda.png' />
                    <a className="navbar-brand"> <span>V</span>eda<sub id='sub'>EventPlanner</sub></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <Link className="nav-link" to="/"> Home<span className="sr-only">(current)</span></Link>
                            </li>
                            <li class="nav-item active">
                                <Link className="nav-link" to="/services"> Services<span className="sr-only">(current)</span></Link>
                            </li>
                            <li class="nav-item active">
                                <Link className="nav-link" to="/about">About <span className="sr-only">(current)</span></Link>
                            </li>
                            <li class="nav-item active">
                                <Link className="nav-link" to="/gallery">Gallery <span className="sr-only">(current)</span></Link>
                            </li>
                            <li class="nav-item active">
                                <Link className="nav-link" to="/contactus">Contactus <span className="sr-only">(current)</span></Link>
                            </li>
                        </ul>
                        <div className='text-xl ' onClick={handleshowMenu}>
                        <div className=' border-2 border-solid border-slate-600 p-1 rounded-full cursor-pointer '>
                            <FaUserAlt />
                        </div>
                        {showMenu && (
                            <div className='absolute right-2 bg-white py-2 px-2 shadow drop-shadow-md flex flex-col'>
                                <Link to={"/Signin"} className='whitwspace-nowrap cursor-pointer'> Signin</Link>

                            </div>


                        )}


                    </div>
                    </div>
                </nav>
            </header>
        </div>
    )
}

export default Navbar