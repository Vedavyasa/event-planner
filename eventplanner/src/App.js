import logo from './logo.svg';
import './App.css';
import Navbar from './Navbar';
import Services from './Services';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from './Home';
import Signin from './Signin';
import Signup from './Signup';
import About from './About';
import Gallery from './Gallery';
import Contactus from './Contactus';
import weddingstages from './weddingstages';
import Batchelor from './Batchelor';
import Birthday from './Birthday';
import Admin from './Admin';
import Adminsignin from './Adminsignin';

function App() {
  return (
    <div className="App">
      
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/Home" exact component={Home} />
          <Route path="/signin" exact component={Signin} />
          <Route path="/signup" exact component={Signup} />
          <Route path="/services" exact component={Services} />
          <Route path="/about" exact component={About}/>
          <Route path="/gallery" exact component={Gallery}/>
          <Route path="/contactus" exact component={Contactus}/>
          <Route path="/weddingstages" exact component={weddingstages}/>
          <Route path="/batchelor" exact component={Batchelor}/>
          <Route path="/birthday" exact component={Birthday}/>
          <Route path="/Admin" exact component={Admin}/>
          <Route path="/Adminsignin" exact component={Adminsignin}/>

          
        </Switch>
      </Router>
      
    </div>
  );
}

export default App;
