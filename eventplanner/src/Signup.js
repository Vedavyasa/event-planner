import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

function Signup() {
    const [firstname, setFirstname] = useState('');
    const history = useHistory();
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSignUp = async (event) => {
        event.preventDefault();
        setFirstname('');
        setLastname('');
        setEmail('');
        setPassword('');

        try {
            const response = await axios.post('http://localhost:3001/Signup', {
                firstname,
                lastname,
                email,
                password,
            });

            if (response.data.message === 'true') {
                alert('Signup successful');
                history.push('/Signin')
            } else {
                alert('Sign up failed');
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className='home'>
            <div className="container mt-5" id='signup'>
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-body" id='up'>
                                <h3 className="card-title text-center" id='h3'>Sign Up</h3><hr />
                                <form onSubmit={handleSignUp}>
                                    <div className="mb-3">
                                        <label htmlFor="firstname" className="form-label">First Name:</label>
                                        <input
                                            type="text"
                                            id="firstname"
                                            value={firstname}
                                            onChange={(e) => setFirstname(e.target.value)}
                                            className="form-control"
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="lastname" className="form-label">Last Name:</label>
                                        <input
                                            type="text"
                                            id="lastname"
                                            value={lastname}
                                            onChange={(e) => setLastname(e.target.value)}
                                            className="form-control"
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email:</label>
                                        <input
                                            type="email"
                                            id="email"
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}
                                            className="form-control"
                                            required
                                        />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Password:</label>
                                        <input
                                            type="password"
                                            id="password"
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}
                                            className="form-control"
                                            required
                                        />
                                    </div>
                                    <button type="submit" className="btnn" id='btns'>Sign Up</button>
                                    <br/><br/>
                                    <Link to="/signin">Already have an account? Sign in</Link>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Signup;