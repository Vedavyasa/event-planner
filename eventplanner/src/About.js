import React from 'react'

function About() {
  return (
    <div className='man'>
      <h1 className='about'>About <span>Us</span></h1>
      <div className='comment'>
        <p>We The HAPPY MOMENTS EVENT PLANNER makes every moments and occasions beautiful and life time memorable as we work as a family member and understands the requirement with the given budget and makes that event very interesting and awesome for always.
          We are working currently in BILASPUR City from 2018 which is located in CHATTISGARH state and give services across state with the convenient and pocket friendly budget.</p>
      </div>
      <div className='box'>
        <div className='company'>
          <img src='veda.png' />
          <h3>Welcome to <span>Veda Eventplanner</span></h3>
          <h6><b>your trusted partner in creating unforgettable events!</b></h6>
          <p>We are a passionate and dedicated team of event planners who specialize in turning your dreams into reality.
            With a wealth of experience and a commitment to excellence, we are here to make your event truly exceptional.</p>
        </div>
        <div className='company'>
          <img src='teamwork.webp' />
          <h3>Our 
            <span>Team</span></h3>
          <h6><b>your trusted partner in creating unforgettable events!</b></h6>
          <p>
            <li>Vedavyasa Yadav (Founder)</li>
            <li>Anil Yadav (Co-Founder)</li>
            <li>Raj kumar (Financier)</li>
             
          </p>
        </div>
        <div className='company'>
          <img src='outparty.jpg' />
          <h3>Outdoor <span>Parties</span></h3>
          <h6><b>your trusted partner in creating unforgettable events!</b></h6>
          <p>After so much hard work, we deserve a little party every once in a while.
          Use these party quotes to remind you that life is a party, and celebration is not only worthwhile but also very necessary.
          </p>
        </div>
      </div>
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3>Contact Us</h3>
              <p>123 Main Street, Karimnagar</p>
              <p>
                <i class="fa-solid fa-envelope"></i>Email: vedaeventplanner@gmail.com</p>
              <p>
                <i class="fa-solid fa-phone"></i>Phone: (+91) 99124**-***</p>
            </div>
            <div class="col-md-6">
              <h3>Follow Us</h3>
              <a href="#" class="social-icon">
                <i class="fa-brands fa-facebook"></i> Facebook</a>
              <a href="#" class="social-icon">
                <i class="fa-brands fa-twitter"></i>Twitter</a>
              <a href="#" class="social-icon">
                <i class="fa-brands fa-instagram"></i>Instagram</a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  )
}

export default About