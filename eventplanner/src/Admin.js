import axios from 'axios';
import React,{useEffect, useState} from 'react'



function Admin() {
  const [data, setData] = useState([]);

  useEffect(() => {
      fetchData();
  }, []);

  async function fetchData() {
      try {
          const response = await axios.get("http://localhost:3001/fetch");
          setData(response.data);
      } catch (error) {
          console.error("Error fetching data", error);
      }
  }

  async function handleDelete(email){
      try{
        const response = await axios.delete(`http://localhost:3001/delete/${email}`)
      fetchData()
      }
      catch (error){
          console.error("Error deleting data", error )
      }
      
  }

//   async function senddata(formdata){
//     console.log(formdata);
//     try{
//         await axios.put('http://localhost:3002/update',formdata);
//         fetchData();
//     }
//     catch(error){
//         console.log('Error in updating the data')
//     }
//   }

  return (
      <div className="container mt-4">
          <table className="table table-bordered">
              <thead className="thead-dark "  >
                  <tr>
                      <th>email</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Delete</th>
                      <th>Update</th>
                  </tr>
              </thead>
              <tbody>
                  {data.map((contact) => (
                      <tr key={contact._email}>
                          <td>{contact.email}</td>
                          <td>{contact.firstname}</td>
                          <td>{contact.lastname}</td>
                          <td><button className='btn btn-warning' onClick={ () => handleDelete(contact.email)}>Delete</button></td>
                          
                      </tr>
                  ))}
              </tbody>
          </table>
          
      </div>
  );
}
export default Admin;