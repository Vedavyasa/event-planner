import React from 'react';


function Contactus() {
  const businessHours = {
    monday: '9:00 AM - 5:00 PM',
    tuesday: '9:00 AM - 5:00 PM',
    wednesday: '9:00 AM - 5:00 PM',
    thursday: '9:00 AM - 5:00 PM',
    friday: '9:00 AM - 5:00 PM',
    saturday: 'Closed',
    sunday: 'Closed',
  };

  return (
    <div>
      <h1 className='contact'>
        <u>Contact <span>Us</span></u>
      </h1>
      <div className='additional'>
        <h3>
          <u>Additional<span>information</span></u>
        </h3>
        <hr />
        <p>Alternative methods for getting in touch with us</p>
        <a>
          <i class="fa-solid fa-envelope"></i> Email: vedavyasayadav@gmail.com
        </a>
        <br />
        <a>
          <i class="fa-solid fa-phone"></i> Mobile no: (+91)99******09
        </a>
      </div>
      <div className='additional'>
        <h3>
          <u>Business<span>Hours</span></u>
        </h3>
        <ul>
          {Object.keys(businessHours).map((day) => (
            <li key={day}>
              {day}: {businessHours[day]}
            </li>
          ))}
        </ul>
      </div>
      <div className='additional'>
      <h3>
          Response Time<span>Expectations</span><hr/>
        </h3>
        <p>'We typically respond to inquiries within 1-2 business days. For urgent matters, please call our support team directly.'</p>
        <p>Urgent<span>Calls: (+91)-7680****09</span></p>
      </div>
      <div className='booking'>
        <h1>For <span> Bookings</span></h1>
        <p>contact the above mentioned deatails </p>
        <h3><span>Thank</span>you</h3>
      </div>
    </div>
  );
}

export default Contactus;
