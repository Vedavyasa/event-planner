import React from 'react'

function Services() {
    return (
        <div >
            <div className='services'>
                <h1 id='our'> Our <span>Services</span></h1>
                <section id="profile">
                    <div class="container">
                        <div class="row">
                            <div class="custumcard col-12 col-md-6 col-lg-4">
                                <div class="card" id='card' style={{ width: '18rem' }}>
                                    <img src="venue.jpeg" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h5 class="card-title"> <i class="fa-solid fa-location-dot"></i> Venue Selection</h5>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="custumcard col-12 col-md-6 col-lg-4">
                                <div class="card" style={{ width: '18rem' }}>
                                    <img src="ent.jpeg" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            <i class="fa-solid fa-music"></i> Entertainment</h5>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>
                            <div class="custumcard col-12 col-md-6 col-lg-4">
                                <div class="card custumcard" style={{ width: '18rem' }}>
                                    <img src="food.jpeg" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h5 class="card-title">
                                        <i class="fa-solid fa-champagne-glasses"></i> Food and Drinks</h5>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                            card's content.</p>
                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </section>
            </div>
            <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3>Contact Us</h3>
              <p>123 Main Street, Karimnagar</p>
              <p>
              <i class="fa-solid fa-envelope"></i>Email: vedaeventplanner@gmail.com</p>
              <p>
              <i class="fa-solid fa-phone"></i>Phone: (+91) 99124**-***</p>
            </div>
            <div class="col-md-6">
              <h3>Follow Us</h3>
              <a href="#"  class="social-icon">
              <i class="fa-brands fa-facebook"></i> Facebook</a>
              <a href="#" class="social-icon">
              <i class="fa-brands fa-twitter"></i>Twitter</a>
              <a href="#" class="social-icon">
              <i class="fa-brands fa-instagram"></i>Instagram</a>
            </div>
          </div>
        </div>
      </footer>
        </div>
    )
}

export default Services