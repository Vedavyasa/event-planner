import React from 'react'
import { Link } from 'react-router-dom/cjs/react-router-dom.min'



function Home() {
  return (
    <div className='decoration'>
      <main>
        <section className='home'>
          <h1>Welcome <span>All</span></h1>
          <h6>Your Event <span>Our Passion</span></h6>
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="img2.jpeg" class="d-block w-100" alt="Wedding Party" />
                <div class="carousel-caption d-none d-md-block">
                  <h5>Wedding Stages</h5>
                  <p>Your Event Our Passion</p>
                  <Link to="/weddingstages" className="btn btn-primary">clickme</Link>
                </div>
              </div>
              <div class="carousel-item">
                <img src="parties.jpg" class="d-block w-100" alt="Bachelore Party" />
                <div class="carousel-caption d-none d-md-block">
                  <h5>Batchelor Parties</h5>
                  <p>Your Event Our Passion</p>
                  <Link to="/batchelor" className="btn btn-primary">clickme</Link>
                </div>
              </div>
              <div class="carousel-item">
                <img src="candle.jpg" class="d-block w-100" alt="Birthday Party" />
                <div class="carousel-caption d-none d-md-block">
                  <h5>Birthday Party</h5>
                  <p>Your Event Our Passion</p>
                  <Link to="/birthday" className="btn btn-primary">clickme</Link>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" ariahidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </section>
      </main>

      <section id="profile">
        <div class="container">
          <div class="row">
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="venue.jpeg" class="card-img-top" alt="..." />
                <div class="card-body">
                  <h5 class="card-title"> <i class="fa-solid fa-location-dot"></i> Venue Selection</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
                </div>
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="ent.jpeg" class="card-img-top" alt="..." />
                <div class="card-body">
                  <h5 class="card-title">
                    <i class="fa-solid fa-music"></i> Entertainment</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
                </div>
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="team.jpg" class="card-img-top" alt="..." />
                <div class="card-body">
                  <h5 class="card-title">
                  <i class="fa-solid fa-people-group"></i>Our Team</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
                </div>
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="thalambralu.webp" class="card-img-top" alt="..." />
                <div class="card-body">
                  <h5 class="card-title">
                  <i class="fa-solid fa-photo-film"></i>Photos&Videos</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
                </div>
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card" style={{ width: '18rem' }}>
                <img src="customfood.jpg" class="card-img-top" alt="..." />
                <div class="card-body">
                  <h5 class="card-title">
                  <i class="fa-solid fa-cake-candles"></i> Custom Foods</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
                </div>
              </div>
            </div>
            <div class="custumcard col-12 col-md-6 col-lg-4">
              <div class="card custumcard" style={{ width: '18rem' }}>
                <img src="food.jpeg" class="card-img-top" alt="..." />
                <div class="card-body">
                  <h5 class="card-title">
                    <i class="fa-solid fa-champagne-glasses"></i> Food and Drinks</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
                </div>
              </div>
            </div>

          </div>

        </div>
      </section>
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3>Contact Us</h3>
              <p>123 Main Street, Karimnagar</p>
              <p>
              <i class="fa-solid fa-envelope"></i>Email: vedaeventplanner@gmail.com</p>
              <p>
              <i class="fa-solid fa-phone"></i>Phone: (+91) 99124**-***</p>
            </div>
            <div class="col-md-6">
              <h3>Follow Us</h3>
              <a href="#"  class="social-icon">
              <i class="fa-brands fa-facebook"></i> Facebook</a>
              <a href="#" class="social-icon">
              <i class="fa-brands fa-twitter"></i>Twitter</a>
              <a href="#" class="social-icon">
              <i class="fa-brands fa-instagram"></i>Instagram</a>
            </div>
          </div>
        </div>
      </footer>

    </div>
  )
}

export default Home