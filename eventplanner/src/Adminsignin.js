import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

function Signin() {
    const [email, setEmail] = useState('');
    const history = useHistory();
    const [password, setPassword] = useState('');


    const handleRegister = async (event) => {
        event.preventDefault();
        setEmail("");
        setPassword("")
        try {
            const response = await axios.get(`http://localhost:3001/Signin/${email}/${password}` );


            if (response.data.message === 'true') {
                alert('Signin successful');
                history.push('/Admin');
            } else {
                alert('Signin failed');
            }
        } catch (error) {
            console.error(error);
        }
    };


    return (
        <div className="login-form" id='signin'>
            <form id='login-form' onSubmit={handleRegister}>
              <h1>Signin</h1><hr/>
                <div className="form-group">
                    <label htmlFor="name">Enter Email:</label>
                    <input
                        type="text"
                        id="Email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                        
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Enter Password:</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </div>
                <button type="submit" className="btn btn-primary">
                   Signin
                </button><br/><br/>
                <button type="button" class="btn btn-primary">Admin</button>
            </form>
        </div>
    );
}


export default Signin;
