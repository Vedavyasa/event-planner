import React from 'react';

const classData = [
    {
        id: 1,image: "thalambralu.webp",
    },{
        id: 2,image: "venue.jpeg",
    },{
        id:3,image: "weddingcollections.jpg",
    },{
        id: 4,image: "beachwedding.jpeg",
    },{
        id: 5,image: "img.jpg",
    },{
        id: 6,image: "istockphoto-928091918-1024x1024.jpg",
    }, {
        id: 7,image: "wdng1.jpg",
    },{
        id: 8, image: "wdng3.jpg",
    },{
        id:9,image:"wdng4.jpg"
    },{
        id:10,image:"wdng5.jpg"
    },{
        id:11,image:"wdng6.jpg"
    },{
        id:12,image:"njy.jpeg"
    },{
        id:13,image:"bdy.jpg"
    },{
        id:14,image:"bdy2.jpg"
    },{
        id:15,image:"bdy3.jpg"
    },{id:16,image:"bdy4.jpg"},{id:17,image:"candle.jpg"},{id:18,image:"wdng8.jpg"}
];

function Gallery() {
    const itemsPerRow = 3; 

    return (
        <div>
            <h1 id='gallery'> Our<span>Gallery</span></h1>
            <div className='gallery'>
                {classData.map((classitem, index) => (
                    <div key={classitem.id} className='class-item'>
                        <img src={classitem.image} alt={`Image ${classitem.id}`} className='img' />
                    </div>
                ))}
            </div>
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <h3>Contact Us</h3>
                            <p>123 Main Street, Karimnagar</p>
                            <p>
                                <i className="fa-solid fa-envelope"></i>Email: vedaeventplanner@gmail.com
                            </p>
                            <p>
                                <i className="fa-solid fa-phone"></i>Phone: (+91) 99124**-***
                            </p>
                        </div>
                        <div className="col-md-6">
                            <h3>Follow Us</h3>
                            <a href="#" className="social-icon">
                                <i className="fa-brands fa-facebook"></i> Facebook
                            </a>
                            <a href="#" className="social-icon">
                                <i className="fa-brands fa-twitter"></i> Twitter
                            </a>
                            <a href="#" className="social-icon">
                                <i className="fa-brands fa-instagram"></i> Instagram
                            </a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default Gallery;
